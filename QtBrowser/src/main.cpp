#include "forms/mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QUrl url("http://www.google.com");
    MainWindow w(url);
    w.show();

    return a.exec();
}
