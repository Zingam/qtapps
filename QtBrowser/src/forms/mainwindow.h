#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWebKit/QWebHistory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT   

private:
    Ui::MainWindow *ui;

    QWebHistory* _history;
    QUrl url;

public:
    explicit MainWindow(QUrl& url, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_Back_clicked();
    void on_pushButton_Forward_clicked();
    void on_pushButton_Go_clicked();
//    void onLinkClicked(QUrl url);
    void onLoadNewPage();
    void onTitleChanged(QString title);
};

#endif // MAINWINDOW_H
