#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtWebKit/QWebSettings>

MainWindow::MainWindow(
        QUrl& url,
        QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::PluginsEnabled, true);

    connect(ui->pushButton_Back,
            SIGNAL(clicked()),
            this,
            SLOT(on_pushButton_Back_clicked()));
    connect(ui->pushButton_Forward,
            SIGNAL(clicked()),
            this,
            SLOT(on_pushButton_Forward_clicked()));
    connect(ui->pushButton_Go,
            SIGNAL(clicked()),
            this,
            SLOT(on_pushButton_Go_clicked()));

    connect(ui->lineEdit_AddressBar,
            SIGNAL(returnPressed()),
            this,
            SLOT(on_pushButton_Go_clicked()));

    connect(ui->webView,
            SIGNAL(loadFinished(bool)),
            this,
            SLOT(onLoadNewPage()));
    // Doesn't work
//    connect(ui->webView,
//            SIGNAL(linkClicked(QUrl)),
//            this,
//            SLOT(onLinkClicked(QUrl)));
    connect(ui->webView,
            SIGNAL(titleChanged(QString)),
            this,
            SLOT(onTitleChanged(QString)));

    ui->webView->load(url);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_Back_clicked()
{
    _history = ui->webView->history();
    _history->back();
}

void MainWindow::on_pushButton_Forward_clicked()
{
    _history = ui->webView->history();
    _history->forward();
}

void MainWindow::on_pushButton_Go_clicked()
{
    url = QUrl::fromUserInput(ui->lineEdit_AddressBar->text());
    ui->webView->load(url);
}

//void MainWindow::onLinkClicked(QUrl url)
//{
//    QString address = url.toString();
//    ui->lineEdit_AddressBar->setText(address);
//    qDebug() << "URL: " << address;
//}

void MainWindow::onLoadNewPage()
{
    QString address = ui->webView->url().toString();
    ui->lineEdit_AddressBar->setText(address);
}

void MainWindow::onTitleChanged(QString title)
{
    this->setWindowTitle(title);
}
