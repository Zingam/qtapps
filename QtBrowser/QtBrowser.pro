#-------------------------------------------------
#
# Project created by QtCreator 2014-07-09T13:50:57
#
#-------------------------------------------------

QT       += core gui
QT += webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtBrowser
TEMPLATE = app


SOURCES += src\main.cpp\
        src\forms\mainwindow.cpp

HEADERS  += src\forms\mainwindow.h

FORMS    += src\forms\mainwindow.ui
