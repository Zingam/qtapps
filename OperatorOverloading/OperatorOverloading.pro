#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T18:10:16
#
#-------------------------------------------------

win32 {
    gcc {
        message("Setting flags for GCC:")
        QMAKE_CXXFLAGS += -std=c++11
    }

    msvc {
        message("Setting flags for MSVC");
    }
}

linux {
    gcc {
        message("Setting flags for GCC:")
        QMAKE_CXXFLAGS += -std=c++11
    }
}

QT       += core

QT       -= gui

TARGET = OperatorOverloading
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += src/main.cpp

HEADERS += \
    src/constants/enums.h
    src/operators/operator.h \
