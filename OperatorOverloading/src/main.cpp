#include <QtCore/QCoreApplication>

#include "constants/enums.h"
#include "operators/operator.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // Test the overloaded operator <<
    qDebug() << "Color::red        <<" << Color::red;
    qDebug() << "Color::green      <<" << Color::green;
    qDebug() << "Color::blue       <<" << Color::blue;
    qDebug() << "Angles::degree000 <<" << Angles::degrees000;
    qDebug() << "Angles::degree001 <<" << Angles::degrees001;
    qDebug() << "Angles::degree002 <<" << Angles::degrees002;
    qDebug() << "Angles::degree180 <<" << Angles::degrees180;
    qDebug() << "Angles::degree181 <<" << Angles::degrees181;
    qDebug() << "Angles::degree182 <<" << Angles::degrees182;
    qDebug() << "Angles::degree275 <<" << Angles::degrees275;

    // Do the above by static_cast
    qDebug() << "Color::red         =" << static_cast<int>(Color::red);
    qDebug() << "Color::green       =" << static_cast<int>(Color::green);
    qDebug() << "Color::blue        =" << static_cast<int>(Color::blue);

    // Test if the overloaded operator << messes with the other overloads
    qDebug() << "QString           <<" << QString("This is a QString").toUtf8().constData();
    qDebug() << "char*             <<" << "This is a C char* string";
    qDebug() << "int               <<" << 1;

    return a.exec();
}
