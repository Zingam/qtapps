Experimental Apps with Qt 5.4
==============

```
    qDebug() << "Experimental projects developed in Qt 5.4";
```

* OperatorOverloading  - Overloading << to support QDebug stream
* QtBrowser            - Qt 5.3 QWebView based browser
* TcpClient            - TcpSocket test - Client (incomplete, non-working)
* TcpServer            - TcpSocket test - Server (incomplete, non-working)


